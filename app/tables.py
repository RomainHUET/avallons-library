from flask_table import Table, Col, LinkCol


class Results(Table):
    id = Col('Id', show=False)
    title = Col('Title')
    availability = Col('Available')
    location = Col('Location')
    reserve = LinkCol('Reserve', 'reserve', url_kwargs=dict(id='id'))


class ResultsEmployee(Table):
    id = Col('Id', show=False)
    title = Col('Title')
    availability = Col('Available')
    location = Col('Location')
    reserve = LinkCol('Reserve', 'reserve', url_kwargs=dict(id='id'))
    edit = LinkCol('Edit', 'edit', url_kwargs=dict(id='id'))


class Reservations(Table):
    id = Col('Id', show=False)
    user_id = Col('User id')
    user_name = Col('User name')
    book_id = Col('Book id')
    book_title = Col('Book title')
