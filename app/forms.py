from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, Form
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo

from app.models import User


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    employee = BooleanField('Are you an employee of the library ?')
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class CreateBookForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    availability = BooleanField('Available')
    location = StringField('Location', validators=[DataRequired()])
    submit = SubmitField('Add Book')


class EditBookForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    availability = BooleanField('Available')
    location = StringField('Location', validators=[DataRequired()])
    submit = SubmitField('Save Changes')


class BookSearchForm(Form):
    search = StringField('')
