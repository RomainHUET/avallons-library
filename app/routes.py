from app import app
from app import db
from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from app.models import User, Book, Reservation
from app.forms import LoginForm, RegistrationForm, CreateBookForm, BookSearchForm, EditBookForm
from app.tables import Results, ResultsEmployee, Reservations


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/index')
@login_required
def index():
    return render_template('index.html', title='Avallon\'s library')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, is_employee=form.employee.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/inventory')
def inventory():
    books = db.session.query(Book).all()
    if not current_user.is_employee:
        table = Results(books)
    else:
        table = ResultsEmployee(books)
    table.border = True
    return render_template('inventory.html', title='Hey c\'est l\'inventaire !', table=table)


@app.route('/add_book', methods=['GET', 'POST'])
def add_book():
    if not current_user.is_employee:
        return redirect(url_for('index'))
    form = CreateBookForm()
    if form.validate_on_submit():
        book = Book(title=form.title.data, availability=form.availability.data, location=form.location.data)
        db.session.add(book)
        db.session.commit()
        return redirect(url_for('inventory'))
    return render_template('add_book.html', title='Add Book', form=form)


@app.route('/search_book', methods=['GET', 'POST'])
def search_book():
    search = BookSearchForm(request.form)
    if request.method == 'POST':
        return search_results(search)

    return render_template('search_book.html', form=search)


@app.route('/results')
def search_results(search):
    results = []
    search_string = search.data['search']

    if search_string:
        qry = db.session.query(Book).filter(Book.title.contains(search_string))
        results = qry.all()

    if not results:
        flash('No results found!')
        return redirect(url_for('search_book'))
    else:
        if not current_user.is_employee:
            flash('not employee')
            table = Results(results)
        else:
            flash('employee')
            table = ResultsEmployee(results)
        table.border = True
        return render_template('results.html', table=table)


def save_changes(book, form):
    book.title = form.title.data
    book.availability = form.availability.data
    book.location = form.location.data
    db.session.commit()


@app.route('/item/<int:id>', methods=['GET', 'POST'])
def edit(id):
    qry = db.session.query(Book).filter(
        Book.id == id)
    book = qry.first()
    if book:
        form = EditBookForm(formdata=request.form, obj=book)
        if request.method == 'POST':
            save_changes(book, form)
            flash('Book updated successfully!')
            return redirect('/inventory')
        return render_template('edit_book.html', form=form)
    else:
        return 'Error loading #{id}'.format(id=id)


@app.route('/reserve/item/<int:id>', methods=['GET', 'POST'])
def reserve(id):
    qry = db.session.query(Book).filter(
        Book.id == id)
    book = qry.first()
    if book:
        if book.availability:
            reservation = Reservation(user_id=current_user.id, user_name=current_user.username,
                                      book_id=book.id, book_title=book.title)
            book.availability = False
            db.session.add(reservation)
            db.session.commit()
            flash('Reservation created successfully!')
            return redirect('/inventory')
        flash('This Book is not available, sorry !')
        return render_template('inventory.html')
    else:
        return 'Error loading #{id}'.format(id=id)


@app.route('/reservations')
def check_reservations():
    if not current_user.is_employee:
        return redirect(url_for('index'))
    reservations = db.session.query(Reservation).all()
    table = Reservations(reservations)
    table.border = True
    return render_template('reservations.html', title='Current reservations', table=table)
